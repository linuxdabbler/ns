PREFIX = /usr
MANDIR = $(PREFIX)/share/man
CONFDIR = $(PREFIX)/share/ns

all:
	@echo Run \'make install\' to install ns

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@mkdir -p $(DESTDIR)$(MANDIR)/man1
	@mkdir -p $(DESTDIR)$(CONFDIR)/ns
	@cp -p ns $(DESTDIR)$(PREFIX)/bin/ns
	@cp -p ns.1 $(DESTDIR)$(MANDIR)/man1
	@cp -p ns.conf $(DESTDIR)$(CONFDIR)/ns.conf
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/ns

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/ns
	@rm -rf $(DESTDIR)$(MANDIR)/man1/ns.1*
	@rm -rf $(DESTDIR)$(CONFDIR)/ns

