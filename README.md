
<h1 style="text-align: center;">NS - notescript</h1>

---

<h3 style="text-align: center;">A simple command line tool for taking notes written in
bash.</h3>

---

<img src="./images/ns-output.png" align="center" height="120px">


NS is simple command line tool written in bash for taking notes in your
favorite terminal based text editor.

All notes are saved in pre-defined note directories and can easily be changed
to suit the user's preference.  By default, the note directory is located in
"\$HOME/.local/share/ns".

---

<img src="./images/ns-help.png" align="center" height="240px">


Whole words can be used as well as flags to invoke commands such as "add" or
"-a", and "edit" or "-e".

NS can be used with or without a configuration file. Default variables are
listed in the script if there is no config file found.  A default config can be
generated in "$HOME/.config/ns/config" by running `ns editconfig` or `ns -ec`

---

<img src="./images/ns-edit.png" align="center" height="360px">


Certain commands such as "edit", "delete", "list", "peek", "view",
"deletefolder", "listfolders", and "usefolder" can take arguments, but also use
fzf as a selection menu if there are no arguments given after the option.

---

<img src="./images/ns-customized-config.png" align="center" height="200px">


You can further configure NS to use exactly the utilites you want it to.
Through the use of the command-line flags and the configuration file, you can
change the behavior of the script to suit your needs.

---

<img src="./images/ns-use-folder.png" align="center" height="360px">


Folders can be utilized for orgainization purposes, if you want to store
personal notes in one folder and work notes in another folder, and never the
two shall meet, this can be done easily with the `ns newfolder` and `ns
usefolder` commands.  The use folder command will change the default note
directory in the config file if it exists or will change it in the script
itself if no config file exists.  It is highly recommended to use a config
file.

---

### DEPENDENCIES
The only dependency this script has by default is fzf which is available in
most linux distribution normal repositories.

If the configuration is customized to use different editors, viewers, etc...
those will become dependencies of the script of course.

---

### INSTALLATION
git clone the repository, cd into the downloaded directory, move ns script into
the $PATH, and move ns.1 into /usr/share/man/man1/

Or simply run `sudo make install` to perform the above task automatically.

